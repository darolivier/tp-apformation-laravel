<div class="mt-6 bg-white shadow-sm rounded-lg divide-y">
    @foreach ($categories as $category)
    <div class="p-6 flex space-x-2">
        <div class="dropdown">
            <div class="flex-1">
                <div class="flex justify-between items-center">

                <a href="/categories/{{ $category->id }}/chirps" class="btn btn-xs btn-info pull-right">{{ $category->name }}</a>
        
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>