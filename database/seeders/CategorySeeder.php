<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();
        Category::factory()->count(3)->create();
      
        // DB::table('categories')->truncate();
    

        // DB::table('categories')->insert([
        //     'name' => 'positive',
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'negative',
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'neutral',
        // ]);
    }
}
