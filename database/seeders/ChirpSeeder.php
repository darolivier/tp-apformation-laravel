<?php

namespace Database\Seeders;

use App\Models\Chirp;
use Database\Factories\ChirpsFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChirpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chirps')->truncate();
        Chirp::factory()->count(50)->createQuietly();
    }
}
